import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    tableContainer: {
        borderRadius: 15,
        maxWidth: 937,
        margin: '10% auto 0 auto',
        color: '#FFFFFF'
    },
    table: {
        minWidth: 650,
    },
    tableCellTitle: {
        padding: '34px 0 0 0px',
        border: 'none',
        fontFamily: 'Lato, sans-serif',
        fontWeight: 'bold',
        fontSize: '16px',
        lineHeight: 1.25,
        color: '#344B60',
        letterSpacing: 0
    },
    addButton: {
        border: 'none',
        backgroundColor: '#E8463D',
        color: '#FFFFFF',
        cursor: 'pointer',
        fontSize: '25px',
        fontWeight: 100,
    },
    addButtonCell: {
        padding: '34px 0 11.45px 33px',
    },
    rounded: {
        display:'block',
        height: 36,
        width: 36,
        borderRadius: '50%',
    },
    textStyle: {
        fontFamily: 'Lato, sans-serif',
        fontSize: '14px',
        color: '#344B60',
    },
    threeDotsBtn: {
        borderRadius: '50%',
        border: 'none',
        backgroundColor: 'white',
        cursor: 'pointer',
    },
    rowhover:{
        paddingLeft: '33px',
        borderTop: '1px solid rgba(224, 224, 224, 1)',
        '&:hover':{
            backgroundColor: '#ECF6FF',
        }
    },
    userPadding: {
        paddingLeft: 'initial'
    },
    thumbnailPadding: {
        padding: '17px 0 17px 33px',
    },
    nameStyle: {
        display: 'flex',
    },
    nameText: {
        padding: '8px 0 0 15px'
    },
    namePadding: {
        paddingLeft: '34px'
    }

});

export default useStyles;