import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
} from '@material-ui/core';
import useStyles from './UserStyles';
import Dropdown from './Dropdown';
import DateConverter from './DateConverter'

const User: React.FC<usert> = (props: usert ) =>{
    const { addUser, users, setUsers} = props;
    const {tableContainer, table, tableCellTitle, addButton, rounded, textStyle, rowhover, addButtonCell, userPadding, thumbnailPadding, nameStyle, nameText, namePadding} = useStyles();

    return (
        <div>
            <TableContainer className={tableContainer} component={Paper}>
                <Table className={table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell className={`${tableCellTitle} ${namePadding}`} align="left">Name</TableCell>
                            <TableCell className={tableCellTitle} align="left">Email</TableCell>
                            <TableCell className={tableCellTitle} align="left">Phone number</TableCell>
                            <TableCell className={tableCellTitle} align="left">Date registered</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell className={addButtonCell} align="right"><button className={`${addButton} ${rounded}`} onClick={addUser}>+</button></TableCell>
                        </TableRow>
                        {users.map((user: UserTypes, index: dropdown)=>{
                            return(
                                <TableRow className={rowhover} key={user.login.uuid}>
                                    <TableCell className={`${thumbnailPadding} ${textStyle}`}  component="th" scope="row"><div className={nameStyle}><img className={rounded} src={user.picture.thumbnail} /><p className={nameText}>{user.name.first} {user.name.last}</p></div></TableCell>
                                    <TableCell className={`${textStyle} ${userPadding}`} align="left">{user.email}</TableCell>
                                    <TableCell className={textStyle}  align="left">{user.phone}</TableCell>
                                    <TableCell className={textStyle} align="left">{DateConverter(user.registered.date)}</TableCell>
                                    <TableCell align="right">
                                        <Dropdown index={index} users={users} setUsers={setUsers}/>
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
}

export default User;