import React from 'react';
import useStyles from "./UserStyles";
import {useState, useRef, useEffect} from "react";
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';

function Dropdown(props: dropdown) {
    const {threeDotsBtn} = useStyles();
    const [dropdown, setDropdown] = useState(false);
    const node = useRef<HTMLDivElement>(null);

    const deleteUser = (index: string) =>{
        const reducedArr = props.users.filter((_: null,itemIndex: string)=>{
            return itemIndex !== index
        })
        props.setUsers([...reducedArr])
    }

    const handleClick = (e: any) => {
        if (node?.current?.contains(e.target)) {
            return;
        }
        setDropdown(false)
    };

    useEffect(() => {
        document.addEventListener("mousedown", handleClick);

        return () => {
            document.removeEventListener("mousedown", handleClick);
        };
    }, []);

    return (
        <React.Fragment>
        <div ref={node}>
            <button className={threeDotsBtn} onClick={() => {
                setDropdown(!dropdown)
            }}>
                <span className='threeDots' />
            </button>
            <div className={`dropdown-body ${dropdown && 'open'}`}>
                <div className='dropdown' onClick={()=>deleteUser(props.index)}>
                    <DeleteForeverOutlinedIcon></DeleteForeverOutlinedIcon>
                    <p>Delete row</p>
                </div>
            </div>
        </div>
        </React.Fragment>
    );
}

export default Dropdown;