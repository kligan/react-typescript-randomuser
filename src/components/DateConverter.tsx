
const DateConverter = (str: string) =>{
    const months = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    const date = new Date(str);
    const formatedDate = date.getDate() + ' ' + months[date.getMonth()] +' '+ date.getFullYear();
    return formatedDate;
}

export default DateConverter;