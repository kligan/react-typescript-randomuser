
const useFetch = async (url: string) => {

    const handleError = ({ err }: useFetch) =>{
        console.warn(err);
        return new Response(JSON.stringify({
            code: 400,
            message: 'network Error'
        }));
    };
    return await fetch(url).catch(handleError)
        .then(function(response) {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response})
        .then((response) => response.json())
        .then((data) => data.results)
};

export default useFetch;