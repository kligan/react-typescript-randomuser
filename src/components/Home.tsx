
import { useEffect, useState} from 'react';
import useFetch from './useFetch';
import User from './User'

    const Home = () => {
    const [users, setUsers] = useState<UserTypes[]>([]);

    useEffect(() =>{
        GetData()
        return () => window.clearInterval()
    },[])

        const GetData = () => {
        useFetch('https://randomuser.me/api/?results=4')
                .then((data) => setUsers([...users, ...data]));
        }

        const addUser =  () =>{
        useFetch('https://randomuser.me/api/?results=1')
          .then((data) => setUsers([...users, ...data]));
        }

    return (
        <div>
            <User {...{setUsers, addUser, users}} />
        </div>
    );
}

export default Home;
