
interface UserTypes {
    name: {
        first: string,
        last: string,
    },
    email: string,
    phone: number,
    registered:{
        date: string,
    }
    picture:{
        thumbnail: string,
    }
    login:{
        uuid: string,
    },
}

interface usert {
    addUser: () => void,
    users: any,
    setUsers: any,
}

interface dropdown {
    users: any,
    setUsers: any,
    index: any,
}

type useFetch = {
    err: string,
}