import './App.css';
import {useState, useEffect} from 'react'
import Home from './components/Home'
import PacmanLoader from 'react-spinners/PacmanLoader'

function App() {
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(()=>{
        setLoading(true);
        setTimeout(()=>{
            setLoading(false);
        },1500)
    },[])

  return (
    <div className="App">
        {
            loading ? (
                <div className='LoadingScreen'>
                    <PacmanLoader size={50} color={"white"} loading={loading} />
                </div>
            ) : (
                <Home/>
            )
        }
    </div>
  );
}

export default App;
