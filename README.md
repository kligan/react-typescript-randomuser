**Getting Started with the project on a local machine**

Requirements

1.Node.js

2.IDE

STEPS


GitLab repository path - git@gitlab.com:kligan/react-typescript-randomuser.git

1. Create a new folder e.g 'FolderX'

2. **git clone git@gitlab.com:kligan/react-typescript-randomuser.git**

Using gitbash or GitHub desktop navigate to the directory path and run the command above

3. Open FolderX in your IDE/code editor

4. **cd react-typescript-randomuser**

Before installing node modules and starting the local server make sure to run the above command

e.g - C:\Tab\Users\FolderX\react-typescript-randomuser

5. **npm install**

This command will install the required node-modules for the project to run

6. **npm start**

The server will run on port 3000

7. **npm test**

Run tests







